#ifndef DATABASE_HH
#define DATABASE_HH

#include <iostream>
#include <string>
#include <pqxx/pqxx>

class Database{

public:
    Database();
    std::vector<std::pair<unsigned int, std::string>> getChannels();
    bool userLogin(const std::string &username, const std::string &password);

private:
    pqxx::connection conn;
};

#endif
