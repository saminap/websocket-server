#include "message.hh"


int Message::calculatePayloadLength(const char *buffer){

    // *****************************************************************
    // * TODO: Should probably store payload length as unsigned long int
    // * because when payloadLength is >= 127 the calculated value will be
    // * 64 bits which will cause unsigned int to overflow
    // *****************************************************************

    // Payload length starts at 2nd byte
    unsigned int payloadLength = (unsigned int)buffer[1] & 127;
    unsigned int calculatedPayloadLength = 0;
    unsigned short int calculatedPayloadStart = 2;

    // If the payload length is 126 then we need to read the next 2 octets
    if(payloadLength == 126){
        for(int i=0; i<2; ++i){
            unsigned int nextByteContent = (unsigned int)buffer[i+calculatedPayloadStart] & 0xff;
            calculatedPayloadLength = calculatedPayloadLength << 8 | nextByteContent;
        }
    }

    // If the payload length is larger than or equal to 127 we need to read the next 8 octets
    else if(payloadLength >= 127){
        for(int i=0; i<8; ++i){
            unsigned int nextByteContent = (unsigned int)buffer[i+calculatedPayloadStart] & 0xff;
            calculatedPayloadLength = calculatedPayloadLength << 8 | nextByteContent;
        }
    }
    else {
        calculatedPayloadLength = payloadLength;
    }

    return calculatedPayloadLength;
}

unsigned short int Message::calculateMaskingKeyStart(unsigned int payloadLength){

    // If payloadLength < 126 then masking key will start at 2nd byte
    unsigned short int maskingKeyStart = 2;

    if(payloadLength == 126)
        maskingKeyStart += 2;
    else if(payloadLength >= 127)
        maskingKeyStart += 8;

    return maskingKeyStart;
}

unsigned short int Message::processCommand(const std::string &message){

    // Message must be in JSON format
    if(message[0] != '{'){
        // TODO: Throw an exception?
        std::cout << "Invalid message format" << std::endl;
    }

    if(message[message.size()-1] != '}'){
        // TODO: Throw an exception?
        std::cout << "Invalid message format" << std::endl;
    }

    return (int)message[2]-'0';
}

Json::Value Message::processPayload(const std::string &message){

    Json::Value payload;
    Json::Reader reader;

    if(!reader.parse(message.c_str(), payload)){
        // TODO: Throw an exception?
        std::cout << "json structure was not found in message" << std::endl;
    }

    return payload;
}

std::string Message::processBuffer(const char *buffer){
    unsigned short int maskingKeyLength = 4;
    unsigned short int maskingKeyStart;
    unsigned int payloadLength;

    // Calculate the payload length
    payloadLength = calculatePayloadLength(buffer);

    // Calculate the masking key start position
    maskingKeyStart = calculateMaskingKeyStart(payloadLength);

    // Reserve payloadLength + 1 for null terminator
    char decoded[payloadLength+1];
    char encoded[payloadLength];
    char mask[payloadLength];

    for(int i=0;i<payloadLength;++i){
        mask[i] = buffer[maskingKeyStart+i];
        encoded[i] = buffer[maskingKeyStart+maskingKeyLength+i];
    }

    for(int i=0;i<payloadLength;++i){
        decoded[i] = encoded[i] ^ mask[i % 4];
    }

    decoded[payloadLength] = '\0';

    return std::string(decoded);
}

std::string Message::generateDataFrame(const std::string &payload){

    size_t payloadStart = 0;
    size_t payloadLengthBytes = 0;
    size_t dataframeLength = 0;

    if(payload.size() > 125 && payload.size() < 65535)
        payloadLengthBytes = 2;
    else if(payload.size() >= 65535)
        payloadLengthBytes = 8;

    payloadStart = payloadLengthBytes+2;
    dataframeLength = payload.size() + payloadStart;
    char *dataframe = new char[dataframeLength+1];

    // Set FIN bit and opcode 0x1
    dataframe[0] = 0b10000001;

    // Set the payload length
    if(payload.size() <= 125)
        dataframe[1] = payload.size() & 0b01111111;
    else
        dataframe[1] = 0b01111110;

    // Set the extended payload length
    for(int i=0; i<payloadLengthBytes; ++i){
        unsigned short int currentByte = payload.size() >> ((payloadLengthBytes-i-1)*8);
        dataframe[i+2] = currentByte & 0xff;
    }

    // Loop the payload content to dataframe
    for(int i=0; i<payload.size(); ++i){
        dataframe[i+payloadStart] = payload.at(i);
    }

    std::string output(dataframe, dataframeLength);
    delete dataframe;

    return output;
}

unsigned short int Message::getOpcode(const char *buffer){
    return buffer[0]&0b1111;
}

bool Message::getMaskBit(const char *buffer){
    return buffer[1]&128;
}
