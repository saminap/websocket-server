#ifndef CLIENT_HH
#define CLIENT_HH

#include "channel.hh"
#include "json/json.h"
#include "message.hh"

#include <iostream>
#include <unordered_set>
#include <unistd.h>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>

class Channel;

class Client{

public:
    Client(int fd);
    void setName(const std::string &name);
    void addChannel(Channel *channel);
    std::unordered_set<Channel *> &getChannels();
    void loginSucceeded(const std::string &username);
    void sendHandshakeData() const;
    void receiveMessage(const Client *sender, const std::string &message, unsigned int channelId) const;
    void notifyAboutConnection(const Client *newClient, unsigned int channelId) const;
    void removeClientFromChannels();
    void loginFailed() const;
    void generateToken();
    const std::string &getName() const;

private:
    int fd;
    std::string name;
    std::string token;
    std::unordered_set<Channel *> channels;

};

#endif
