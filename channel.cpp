#include "channel.hh"

Channel::Channel(unsigned int id, const std::string &name): id(id), name(name){
}

void Channel::addUser(Client *client){
    members.insert(client);
    client->addChannel(this);
}

void Channel::deleteUser(Client *client){
    members.erase(client);
}

void Channel::broadcastMessage(Client *sender, const std::string &message){
    if(members.find(sender) != members.end()){
        std::unordered_set<Client*>::const_iterator membersIter = members.begin();

        while(membersIter != members.end()){

            // Send the message to everyone except the sender
            if(*membersIter != sender)
                (*membersIter)->receiveMessage(sender, message, id);

            ++membersIter;
        }
    }
}

void Channel::broadcastUserJoined(const Client *client) const{
    std::unordered_set<Client*>::const_iterator membersIter = members.begin();

    while(membersIter != members.end()){

        // Send the message to everyone except the sender
        if(*membersIter != client)
            (*membersIter)->notifyAboutConnection(client, id);

        ++membersIter;
    }
}

const std::unordered_set<Client *> &Channel::getMembers() const{
    return members;
}

const std::string &Channel::getName() const{
    return name;
}

unsigned int Channel::getId() const{
    return id;
}
