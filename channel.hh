#ifndef CHANNEL_HH
#define CHANNEL_HH

#include "client.hh"

#include <iostream>
#include <string>
#include <unordered_set>

class Client;

class Channel{
public:
    Channel(unsigned int id, const std::string &name);
    void addUser(Client *client);
    void deleteUser(Client *client);
    void broadcastMessage(Client *sender, const std::string &message);
    void broadcastUserJoined(const Client *client) const;
    const std::unordered_set<Client*> &getMembers() const;
    const std::string &getName() const;
    unsigned int getId() const;

private:
    unsigned int id;
    std::string name;
    std::unordered_set<Client*> members;
};

#endif
