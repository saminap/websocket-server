#ifndef MESSAGE_HH
#define MESSAGE_HH

#include "json/json.h"

#include <cstring>
#include <string>
#include <iostream>

namespace Message{
    int calculatePayloadLength(const char *buffer);
    unsigned short int calculateMaskingKeyStart(unsigned int payloadLength);
    unsigned short int processCommand(const std::string &message);
    Json::Value processPayload(const std::string &message);
    std::string processBuffer(const char *buffer);
    std::string generateDataFrame(const std::string &payload);
    unsigned short int getOpcode(const char *buffer);
    bool getMaskBit(const char *buffer);
}

#endif
