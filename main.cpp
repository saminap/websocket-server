
#include "server.hh"

#include <iostream>

int main(){

    Server server(8088);
    server.initialize();
    server.start();

    return 0;
}
