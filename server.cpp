
#include "server.hh"

Server::Server(int port): port(port), database(){
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(port);
}

void Server::initialize(){

    server_fd = socket(AF_INET, SOCK_STREAM, 0);

    if(server_fd < 0){
        std::cout << "Error creating socket" << std::endl;
        // TODO: throw an exception maybe?
    }

    if(bind(server_fd, (struct sockaddr *)&server, sizeof(server)) < 0){
        std::cout << "Could not bind server socket" << std::endl;
        // TODO: Throw an exception maybe?
    }

    epoll_fd = epoll_create1(0);
    events = (struct epoll_event*)calloc (EPOLL_QUEUE_LEN, sizeof(struct epoll_event));

    setNonBlocking(server_fd);
    listen(server_fd, 5);
    createEpollEvent(server_fd, EPOLLIN | EPOLLPRI | EPOLLERR | EPOLLHUP);

    std::vector<std::pair<unsigned int, std::string>> databaseChannels = database.getChannels();

    for(std::pair<unsigned int, std::string> channel : databaseChannels){
        Channel *newChannel = new Channel(channel.first, channel.second);
        channels.insert(std::make_pair(channel.first, newChannel));
    }
}

Server::~Server(){
    close(server_fd);
    free(events);
}

void Server::start(){

    while(true){
        std::cout << "Listening for connections" << std::endl;

        int numberOfDescriptors = epoll_wait (epoll_fd, events, EPOLL_QUEUE_LEN, -1);

        for(int i=0; i<numberOfDescriptors; ++i){
            if(events[i].data.fd == server_fd)
                handleServerRequest();
            else{
                if(events[i].events & EPOLLIN)
                    handleIncomingMessage(&events[i]);
            }
        }
    }
}

void Server::setNonBlocking(int fd) const{
    int flags;
    flags = fcntl (fd, F_GETFL, 0);
    flags |= O_NONBLOCK;
    fcntl (fd, F_SETFL, flags);
}

void Server::createEpollEvent(int fd, uint32_t events){
    struct epoll_event event;
    event.events = events;
    event.data.fd = fd;

    epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &event);
}

void Server::handleServerRequest(){
    struct sockaddr_in client;
    int client_fd;
    socklen_t addrlen = (socklen_t)sizeof(client);
    char ip[INET_ADDRSTRLEN];

    client_fd = accept(server_fd, (struct sockaddr *) &client, &addrlen);
    if(client_fd == -1){
        std::cout << "Accept failed" << std::endl;
        // TODO: throw an exception?
    }

    inet_ntop(AF_INET, &client.sin_addr, ip, INET_ADDRSTRLEN);

    std::cout << "connection from ip: " << ip << std::endl;

    Client *newClient = new Client(client_fd);

    connections.insert(std::make_pair(client_fd, newClient));
    channels.find(1)->second->addUser(newClient);
    channels.find(2)->second->addUser(newClient);
    setNonBlocking(client_fd);
    createEpollEvent(client_fd);
}

void Server::handleIncomingMessage(struct epoll_event *event){

    char buffer[BUFFER_SIZE];
    char method[4];
    char get[] = "GET";

    bzero(buffer, BUFFER_SIZE);

    if(read(event->data.fd, buffer, BUFFER_SIZE) > 0){

        strncpy (method, buffer, 3);
        method[3] = '\0';

        // New connection attempt
        if(strcmp(method, get) == 0){
            std::string sendBuffer;
            std::string calculatedMagicKey;
            std::string webSocketKey;

            webSocketKey = parseWebSocketKey(buffer);
            sendBuffer.append("HTTP/1.1 101 Switching Protocols\r\n");
            sendBuffer.append("Upgrade: websocket\r\n");
            sendBuffer.append("Connection: Upgrade\r\n");
            sendBuffer.append("Sec-WebSocket-Accept: ");
            sendBuffer.append(calculateMagicKey(webSocketKey));
            sendBuffer.append("\r\n\r\n");

            write(event->data.fd, sendBuffer.c_str(), sendBuffer.size());
        }

        // Something else (such as a new message)
        else {

            // Mask bit should always be set when communicating from client to server
            if(Message::getMaskBit(buffer) && Message::getOpcode(buffer) != 8){
                Json::Value payload = Message::processPayload(Message::processBuffer(buffer));
                Json::FastWriter fastWriter;
                int command = 0;

                try {
                    command = std::stoi(fastWriter.write(payload["type"]));
                }
                catch(std::exception e){
                    // TODO: terminate connection or inform about faulty message?
                    std::cout << "exception caught " << e.what() << std::endl;
                }

                switch(command){
                    case 1:{
                        std::string username = fastWriter.write(payload["username"]);
                        std::string password = fastWriter.write(payload["password"]);

                        username.erase(remove(username.begin(), username.end(), '\"' ), username.end());
                        username.erase(remove(username.begin(), username.end(), '\n' ), username.end());
                        password.erase(remove(password.begin(), password.end(), '\"' ), password.end());
                        password.erase(remove(password.begin(), password.end(), '\n' ), password.end());

                        // Create token for this session and send back channels the client is currently in
                        if(authenticateUser(username, password)){
                            Client *client = connections.find(event->data.fd)->second;
                            client->loginSucceeded(username);
                        }
                        // Login failed (incorrect username-password combo or user not found), notify client
                        else {
                            connections.find(event->data.fd)->second->loginFailed();
                        }
                        break;
                    }
                    case 2:{
                        int channelId = 0;
                        std::string channel = fastWriter.write(payload["channel"]);
                        std::string message = fastWriter.write(payload["message"]);

                        channel.erase(remove(channel.begin(), channel.end(), '\"' ), channel.end());
                        channel.erase(remove(channel.begin(), channel.end(), '\n' ), channel.end());
                        message.erase(remove(message.begin(), message.end(), '\"' ), message.end());
                        message.erase(remove(message.begin(), message.end(), '\n' ), message.end());

                        try{
                            channelId = std::stoi(channel);
                        }
                        catch(std::exception e){
                            std::cout << "caught an exception " << e.what() << std::endl;
                            return;
                        }

                        std::unordered_map<unsigned int, Channel*>::iterator targetChannel = channels.find(channelId);

                        // Check that the channel exists
                        if(targetChannel != channels.end())
                            targetChannel->second->broadcastMessage(connections.find(event->data.fd)->second, message);

                        break;
                    }
                }
            }
            else {
                // If the mask bit is not set the connection should terminate
                closeConnection(event);
            }
        }
    }
    else{
        // The client has closed the connection
        closeConnection(event);
    }
}

std::string Server::parseWebSocketKey(const char *buffer){
    std::string webSocketKey = "";
    std::string bufferStr(buffer);
    std::size_t secWebSocketKey = bufferStr.find("Sec-WebSocket-Key:");

    if(secWebSocketKey != std::string::npos){
        std::size_t start = bufferStr.find(" ", secWebSocketKey)+1;
        std::size_t lineEnd = bufferStr.find("\r\n", start);
        webSocketKey = bufferStr.substr(start, lineEnd-start);
    }

    return webSocketKey;
}

std::string Server::calculateMagicKey(const std::string &webSocketKey){
    unsigned char shaMessageDigest[SHA_DIGEST_LENGTH];
    BIO *b64Memory, *b64;
    BUF_MEM *b64Ptr;
    std::string magicKey;

    b64 = BIO_new(BIO_f_base64());
    b64Memory = BIO_new(BIO_s_mem());
    b64 = BIO_push(b64, b64Memory);

    std::string concatenated = webSocketKey;
    concatenated.append(GUID);

    SHA1((unsigned char *)concatenated.c_str(), concatenated.size(), shaMessageDigest);

    BIO_write(b64, shaMessageDigest, SHA_DIGEST_LENGTH);
    BIO_flush(b64);
    BIO_get_mem_ptr(b64, &b64Ptr);

    char *buffer = (char *)malloc(b64Ptr->length);
    memcpy(buffer, b64Ptr->data, b64Ptr->length-1);
    buffer[b64Ptr->length-1] = 0;

    BIO_free_all(b64);

    magicKey = buffer;

    free(buffer);
    return magicKey;
}

void Server::closeConnection(struct epoll_event *event){
    Client *client = connections.find(event->data.fd)->second;
    connections.erase(event->data.fd);
    client->removeClientFromChannels();
    delete client;
    close(event->data.fd);
}

bool Server::authenticateUser(const std::string &username, const std::string &password){
    return database.userLogin(username, password);
}
