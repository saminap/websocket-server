
CXX=g++
CPP=$(wildcard *.cpp)
O=$(CPP:.cpp=.o)
INCLUDE=-lcrypto -lssl -lpqxx
CXXFLAGS=-std=gnu++11 -m64 -O3

.PHONY: clean default

default: $(O)
	$(CXX) $(CXXFLAGS) $^ $(INCLUDE)

%.o: %.cpp %.hh
	$(CXX) $(CXXFLAGS) -c -o $@ $< $(INCLUDE)

clean:
	rm *.o
	rm *.out
