#ifndef SERVER_HH
#define SERVER_HH

#define BUFFER_SIZE 1024
#define EPOLL_QUEUE_LEN 64
#define GUID "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"

#include "channel.hh"
#include "client.hh"
#include "database.hh"
#include "json/json.h"
#include "message.hh"

#include <algorithm>
#include <arpa/inet.h>
#include <iostream>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <string>
#include <openssl/sha.h>
#include <openssl/hmac.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/buffer.h>
#include <cstdio>
#include <cstring>
#include <stdint.h>
#include <sys/epoll.h>
#include <bitset>
#include <fcntl.h>
#include <unordered_map>
#include <unordered_set>

class Server{

public:
    Server(int port);
    ~Server();
    void initialize();
    void start();

private:
    int port;
    int server_fd;
    int epoll_fd;
    struct epoll_event *events;
    struct sockaddr_in server;
    Database database;
    std::unordered_map<unsigned int, Channel*> channels;
    std::unordered_map<int, Client *> connections;

    void setNonBlocking(int fd) const;
    void createEpollEvent(int fd, uint32_t events=EPOLLIN | EPOLLET);
    void handleServerRequest();
    void handleIncomingMessage(struct epoll_event *event);
    std::string parseWebSocketKey(const char *buffer);
    std::string calculateMagicKey(const std::string &webSocketKey);
    void closeConnection(struct epoll_event *event);
    bool authenticateUser(const std::string &username, const std::string &password);
};

#endif
