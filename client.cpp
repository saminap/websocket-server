
#include "client.hh"

Client::Client(int fd): fd(fd){
}

void Client::setName(const std::string &name){
    this->name = name;
}

void Client::addChannel(Channel *channel){
    channels.insert(channel);
}

std::unordered_set<Channel *> &Client::getChannels(){
    return channels;
}

void Client::loginSucceeded(const std::string &username){
    generateToken();
    setName(username);
    sendHandshakeData();

    for(std::unordered_set<Channel*>::const_iterator channelIter = channels.begin();
        channelIter != channels.end(); ++channelIter){

            (*channelIter)->broadcastUserJoined(this);
        }
}

void Client::sendHandshakeData() const{
    Json::Value response;
    Json::FastWriter fastWriter;
    std::string output;

    response["token"] = token;
    response["type"] = 1;
    response["status"] = 1;

    int channelIterIndex = 0;
    int membersIterIndex = 0;

    for(std::unordered_set<Channel*>::const_iterator channelIter = channels.begin();
        channelIter != channels.end(); ++channelIter, ++channelIterIndex){
        response["channels"][channelIterIndex]["name"] = (*channelIter)->getName();
        response["channels"][channelIterIndex]["id"]   = (*channelIter)->getId();

        for(std::unordered_set<Client *>::const_iterator membersIter = (*channelIter)->getMembers().begin();
            membersIter != (*channelIter)->getMembers().end(); ++membersIter, ++membersIterIndex){
            response["channels"][channelIterIndex]["users"][membersIterIndex] = (*membersIter)->getName();
        }

        membersIterIndex = 0;
    }

    output = fastWriter.write(response);
    output = Message::generateDataFrame(output);

    write(fd, output.c_str(), output.size());

}

void Client::receiveMessage(const Client *sender, const std::string &message, unsigned int channelId) const{
    Json::Value response;
    Json::FastWriter fastWriter;
    std::string output;

    response["type"] = 2;
    response["from"] = sender->getName();
    response["channel"] = channelId;
    response["message"] = message;

    output = fastWriter.write(response);
    output = Message::generateDataFrame(output);

    write(fd, output.c_str(), output.size());
}

void Client::notifyAboutConnection(const Client *newClient, unsigned int channelId) const{
    Json::Value response;
    Json::FastWriter fastWriter;
    std::string output;

    response["type"] = 3;
    response["username"] = newClient->getName();
    response["channel"] = channelId;

    output = fastWriter.write(response);
    output = Message::generateDataFrame(output);

    write(fd, output.c_str(), output.size());
}

void Client::removeClientFromChannels(){
    std::unordered_set<Channel*>::iterator channel_iter = channels.begin();

    while(channel_iter != channels.end()){
        (*channel_iter)->deleteUser(this);
        ++channel_iter;
    }
}

void Client::loginFailed() const{
    Json::Value response;
    Json::FastWriter fastWriter;
    std::string output;

    response["type"] = 1;
    response["status"] = 0;

    output = fastWriter.write(response);
    output = Message::generateDataFrame(output);

    write(fd, output.c_str(), output.size());
}

void Client::generateToken(){
    // TODO: Some sort of hash from random value could be created
    token = "test";
}

const std::string &Client::getName() const{
    return name;
}
