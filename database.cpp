#include "database.hh"

Database::Database(): conn("dbname=websocket_chat user=sami password= hostaddr=127.0.0.1 port=5432"){
}

std::vector<std::pair<unsigned int, std::string>> Database::getChannels(){
    pqxx::nontransaction query(conn);
    pqxx::result result(query.exec("SELECT * FROM channel"));
    std::vector<std::pair<unsigned int, std::string>> channels;

    for (pqxx::result::const_iterator c = result.begin(); c != result.end(); ++c) {
        channels.push_back(std::make_pair(c[0].as(unsigned()), c[1].as(std::string())));
    }

    return channels;
}

bool Database::userLogin(const std::string &username, const std::string &password){
    pqxx::work work(conn);
    pqxx::result result;

    conn.prepare("usercheck", "SELECT id FROM users WHERE name = $1 AND password = $2");
    result = work.prepared("usercheck")(username)(password).exec();
    work.commit();

    return result.size();
}
